# xiaomi-m365-arduino-turn-signals

Software that provides interface to left/right turn signals, headlight toggle, and horn from 4 grip switches (arranged 2x2). For use in a microcontroller in an electic scooter. Designed for the Xiaomi Mijia M365 but really can work on many microcontrollers installed in any scooter, provided there's a 12V power source.

Handles debouncing and correcting crosstalk issue with the M365's controls.

Currently tested on the Particle Photon or the Arudino Nano.

Demo video: [https://www.youtube.com/watch?v=9uAyNbkA-as](https://www.youtube.com/watch?v=9uAyNbkA-as)

More info: [http://nelsonware.com/blog/2019/02/24/xiaomi-mijia-m365-grip-buttons-blinkers-and-horn.html](http://nelsonware.com/blog/2019/02/24/xiaomi-mijia-m365-grip-buttons-blinkers-and-horn.html)

# Building for Arduino Nano

`platformio run` / `platformio run -t upload`

# Building for Particle Photon

`particle compile photon src/main.ino` / `particle flash yourdevicename src/main.ino`
